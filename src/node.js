RAD = 10;

class Node {
  constructor(name, x, y) {
    this.x = x;
    this.y = y;
    this.name = name;
    this.nodes = []
  }
  
  addNode(node) {
    this.nodes.push(node);
  }
  
  draw() {
    text(this.name, this.x + 10, this.y + 4)
    for (let i = 0; i < this.nodes.length; i += 1) {
      line(this.x, this.y, this.nodes[i].x, this.nodes[i].y);
      this.nodes[i].draw();
    }
    ellipse(this.x, this.y, RAD)
  }
}
