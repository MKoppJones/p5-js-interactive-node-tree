nodes = []
function setup() {
  createCanvas(400, 400);
  nodes.push(new Node('SP5', 10, 10))
  nodes.push(new Node('SP10', 10, 100))
  nodes[0].addNode(new Node('SP6', 50, 40))
}

function mouseClicked() {
  print(mouseX, mouseY);
}

function draw() {
  background(220);
  for (let i = 0; i < nodes.length; i += 1) {
    curr = nodes[i];
    next = nodes[i + 1];
    if (next) {
      line(curr.x, curr.y, next.x, next.y);
    }
    nodes[i].draw()
  }
}